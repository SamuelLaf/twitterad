/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samlaf.twitterad.tests;

import com.samlaf.twitterad.beans.TweetData;
import com.samlaf.twitterad.business.TwitterManager;
import com.samlaf.twitterad.persistance.TweetDAOImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 *
 * @author Sam
 */
public class TwitterADTest {

    private static final Logger LOG = Logger.getLogger(TwitterADTest.class.getName());
    
    private final String url = "jdbc:mysql://localhost:3306/TWITTERAD?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "student";
    private final String password = "secret";
    
    private final TwitterManager TM = new TwitterManager();
    
    //test create tweet. requires TwitterManager to get instance of a user to store
    // in the bean, which will be translated to an id when stored in db.
    @Test
    public void testCreateTweet() throws SQLException{
        User ken;
        try {
            ken = TM.getUserDetails(29476424l);
        } catch (TwitterException e) {
            throw new RuntimeException("failed to get user from userId ", e);
        }
        TweetData tweet = new TweetData();
        
        tweet.setId(12345l);
        tweet.setUser(ken);
        tweet.setText("heyheyhey");
        tweet.setIsLiked(true);
        tweet.setIsRetweeted(true);
        
        TweetDAOImpl dao = new TweetDAOImpl();
        assert(dao.createTweet(tweet));
    }
    
    
    //test get all tweets
    @Test
    public void testGetSavedTweet() throws SQLException, TwitterException{
        User ken;
        try {
            ken = TM.getUserDetails(29476424l);
        } catch (TwitterException e) {
            throw new RuntimeException("failed to get user from userId ", e);
        }
        TweetData tweet = new TweetData();
        
        tweet.setId(12345l);
        tweet.setUser(ken);
        tweet.setText("heyheyhey");
        tweet.setIsLiked(true);
        tweet.setIsRetweeted(true);
        
        TweetDAOImpl dao = new TweetDAOImpl();
        dao.createTweet(tweet);
        ArrayList<TweetData> list = dao.getSavedTweets();
        
        assertEquals(tweet,list.get(0));
    }
    
    
    //seeding provided in examples by Bartosz Majsak.
    // Required to do before AND after to ensure that the db which the app will
    //use is clean.
    @Before
    @After
    public void seedDatabase() {
        LOG.info("Seeding Database");
        final String seedDataScript = loadAsString("CreateTweetTable.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabase method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
}
