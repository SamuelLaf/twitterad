-- This file must have ran at least once before running the program 
-- This script needs to run only once

DROP DATABASE IF EXISTS TWITTERAD;
CREATE DATABASE TWITTERAD;
USE TWITTERAD;

DROP USER IF EXISTS student@localhost;
CREATE USER student@'localhost' IDENTIFIED WITH mysql_native_password BY 'secret' REQUIRE NONE;
GRANT ALL ON TWITTERAD.* TO student@'localhost';


FLUSH PRIVILEGES;
