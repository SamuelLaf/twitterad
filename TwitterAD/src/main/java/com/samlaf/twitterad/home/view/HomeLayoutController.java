package com.samlaf.twitterad.home.view;

/**
 * FXML Controller class
 *
 * @author Sam
 */

import com.samlaf.twitterad.beans.TweetData;
import com.samlaf.twitterad.business.TwitterManager;
import com.samlaf.twitterad.persistance.TweetDAOImpl;
import com.samlaf.twitterad.tweet.view.TweetLayoutController;
import com.samlaf.twitterad.view.RootLayoutController;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.User;

/** FXML Controller class. this class is managing the IO of the home page aka the
 *  the timeline page. it will process the request from the user to show a timeline
 * and switch panes if he tries to retweet with comment or to show a user's details
 * 
 * 
 * @author Sam
 */
public class HomeLayoutController {
    
    private final TwitterManager TM = new TwitterManager();
    private final ArrayList<VBox> timeline = new ArrayList<>();
    private final ArrayList<VBox> mentionsTimeline = new ArrayList<>();
    private final ArrayList<VBox> savedTimeline = new ArrayList<>();
    private final ArrayList<VBox> searchTimeline = new ArrayList<>();
    private Boolean timelineDisplayed = false;
    private Boolean mentionsDisplayed = false;
    private Boolean savedDisplayed = false;
    private Boolean searchDisplayed = false;
    private final Locale currentLocale = new Locale("en", "CA");
    private int timelinePage = 1;
    private int mentionsPage = 1;
    private RootLayoutController rootController;
    
    private TweetData currentlyInteractingWith;
    private Boolean isFollowingCurrentlyInteractingWith;
    
    private static final Logger LOG = LoggerFactory.getLogger(HomeLayoutController.class);

    @FXML // fx:id="showTimelineBtn"
    private Button showTimelineBtn; // Value injected by FXMLLoader

    @FXML // fx:id="showMentionsBtn"
    private Button showMentionsBtn; // Value injected by FXMLLoader

    @FXML // fx:id="timelineSearch"
    private TextField timelineSearch; // Value injected by FXMLLoader

    @FXML // fx:id="timelinePane"
    private VBox timelinePane; // Value injected by FXMLLoader
    
    @FXML
    private Button showSavedTweets;
    
    @FXML
    private VBox homePane;
    
    @FXML
    private TextArea tweetTextArea;

    @FXML
    private Label errorLabel;

    @FXML
    private Button sendTweetButton;
    
    @FXML
    private Button goToConvoBtn;
    
    @FXML
    private Button followBtn;
    
    @FXML
    private BorderPane retweetPane;
    
    @FXML
    private Circle userProfileImage;

    @FXML
    private AnchorPane showUserPane;
    
    @FXML
    private Label showUserName;

    @FXML
    private Label showUserScreenName;

 
    /** go to conversation handler from the user details pane. will switch pane 
     * from root to show the conversation with the user.
     * 
     * @param event 
     */
    @FXML
    void goToConvo(ActionEvent event) {
        rootController.switchToConvo(currentlyInteractingWith.getUser().getId());
    }

    /** display the search result in the timeline pane
     * 
     * @param event 
     */
    @FXML
    void searchHandler(ActionEvent event) {
        LOG.debug("search handle");
        loadSearchTimeline();
        displaySearchTimeline();
        updateButtonText();
    }

    /** this method is used to post a tweet. It is implemented in this class and
     * in the sendTweet controller class since we need to be able to send retweets
     * the same way we send normal tweets, adding a URL at the end of it, which 
     * requires the string to be shorter than normal
     * 
     * @param event 
     */
    @FXML
    void sendTweetHandler(ActionEvent event) {
        String text = tweetTextArea.getText();
        String handle = currentlyInteractingWith.getUser().getScreenName();
        Long tweetId = currentlyInteractingWith.getId();
        String retweetingURL = " " + "twitter.com/" + handle + "/status/" + tweetId;
        if(text.length() > 200){
            errorLabel.setVisible(true);
        }
        try {
            TM.SendTweet(text + retweetingURL);
            errorLabel.setVisible(false);
            hideAllBut(homePane);
        } catch (TwitterException ex) {
            errorLabel.setVisible(true);
            LOG.info("failed to send tweet",ex);
        }
    }
    
    /**
     *  back to timeline handler to hide all but the homePane
     * @param event 
     */
    @FXML
    void backToTimeLine(ActionEvent event) {
        hideAllBut(homePane);
    }
    
    /**
     * event handler for the click of show mentions or to get 20 more tweets. 
     * will manage the process of getting the user's mentions timeline
     * 
     * @param event 
     */
    @FXML
    void showMentions(ActionEvent event) {
        loadMentionsTimeline();
        displayMentionTimeline();
        updateButtonText();
        
        
    }
    
    @FXML
    void showSaved(ActionEvent event) {
        savedTimeline.clear();
        loadSavedTimeline();
        displaySavedTimeline();
        updateButtonText();
        
        LOG.debug("done loading saved tweets");
    }
    

    /**
     * event handler for the click of show timeline. will manage the process of
     * getting the user's timeline
     * @param event 
     */
    @FXML
    void showTimeline(ActionEvent event) {
        loadTimeline();
        displayTimeline();
        updateButtonText();
        
        LOG.debug("timeline btn clicked");
    }
    
    
    /** this is the first method of the process of displaying the timeline.
     * it will get from twitter using the twitter4j library and load the result
     *  into a list. it will call addToTimeline on every tweet in the list
     *  
     */
    private void loadTimeline() {
        List<TweetData> timelineData;
        
        
        try {
            timelineData = TM.getTL(timelinePage);
            LOG.debug("adding tweets to timeline...");
            for(TweetData tweet : timelineData){
                addToTimeline(tweet,timeline);
            }
            showTimelineBtn.setText("get 20 more");
            showMentionsBtn.setText("Show mentions");
            timelinePage +=1;
            mentionsPage = 1;
            mentionsTimeline.clear();
            savedTimeline.clear();
        } catch (TwitterException ex) {
            LOG.error("failed to get timeline", ex);
        }
    }
    
    private void loadSavedTimeline(){
        List<TweetData> timelineData;
        
        try {
            TweetDAOImpl dao = new TweetDAOImpl();
            timelineData = dao.getSavedTweets();
            LOG.debug("adding tweets to timeline...");
            for(TweetData tweet : timelineData){
                addToTimeline(tweet,savedTimeline);
            }
            showTimelineBtn.setText("Show Timeline");
            showMentionsBtn.setText("Show mentions");
            timelinePage =1;
            mentionsPage = 1;
            mentionsTimeline.clear();
            timeline.clear();
        } catch (SQLException ex) {
            LOG.error("failed to get saved timeline", ex);
        } catch (TwitterException ex) {
            LOG.error("failed to get saved timeline", ex);
        }
    }

    /** this is the first method of the process of displaying the timeline mentions.
     * it will get from twitter using the twitter4j library and load the result
     *  into a list. it will call addToTimeline on every tweet in the list.
     * page determines which set of 20 tweets to get. reset when requesting 
     * different timeline
     *  
     */
    private void loadMentionsTimeline() {
        List<TweetData> timelineMentionsData;
        try {
            timelineMentionsData = TM.getMentionsTL(mentionsPage);
            LOG.debug("adding tweets to mention timeline...");
            for(TweetData tweet : timelineMentionsData){
                addToTimeline(tweet,mentionsTimeline);
            }
            mentionsPage += 1;
            timelinePage = 1;
            timeline.clear();            
            savedTimeline.clear();
        } catch (TwitterException ex) {
            LOG.error("failed to get timeline", ex);
        }
    }
    
    /** This method takes tweet as input and creates the version that the user
     * will see, set the event handlers for that tweet
     * and loads that version into the given timeline.
     * page determines which set of 20 tweets to get. reset when requesting 
     * different timeline
     * 
     * @param tweet 
     */
    private void addToTimeline(TweetData tweet, ArrayList<VBox> timeline) {
        User user = tweet.getUser();
        
        String name = user.getName();
        String profileURL = user.get400x400ProfileImageURL();
        String text = tweet.getText();
        Image profilePic = new Image(profileURL);
        
        
        try{
            
            FXMLLoader load = new FXMLLoader();
            
            Locale location = new Locale("en", "CA");
            
            load.setLocation(getClass().getResource("/fxml/tweetLayoutFXML.fxml"));
            load.setResources(ResourceBundle.getBundle("MessagesBundle", location));
            VBox tweetBox =  load.load();
            
            TweetLayoutController controller = load.getController();
            
            
            controller.setTweet(tweet);
            
            controller.setProfileImage(new ImagePattern(profilePic));
            controller.setTweetText(text);
            controller.setTweetName(name);
            
            controller.setProfileCircleAction(e -> showUserDetails(new ImagePattern(profilePic),tweet));
            
            controller.setCounts();
            
            if(timeline == savedTimeline){
                controller.disableSaveBtn();
            }   
            
            MenuItem rtWithTextBtn = controller.getRtWithTextBtn();
            rtWithTextBtn.setOnAction(e -> showRetweetPaneForId(tweet));
            
            timeline.add(tweetBox);
            
        }catch(IOException e){
            LOG.error(null, e);
            Platform.exit();
        }
        
        
        
    }
    
    /** This method takes tweet as input and creates the version that the user
     * will see, set the event handlers for that tweet
     * and loads that version into savedMentionsTimeline.
     * 
     * @param tweet 
     */
    
    
    /** last method in the process of showing timeline, adds all member of list 
     * to the pane on screen.
     * 
     */
    private void displayTimeline() {
        mentionsDisplayed = false;
        timelineDisplayed = true;
        savedDisplayed = false;
        searchDisplayed = false;
        LOG.debug("displaying timeline");
        timelinePane.getChildren().clear();
        for(VBox box : timeline){
            timelinePane.getChildren().add(box);
        }
    }
    
    /** last method in the process of showing timeline, adds all member of list 
     * to the pane on screen.
     * 
     */
    private void displayMentionTimeline() {
        mentionsDisplayed = true;
        timelineDisplayed = false;
        savedDisplayed = false;
        searchDisplayed = false;
        LOG.debug("displaying mentions timeline");
        timelinePane.getChildren().clear();
        for(VBox box : mentionsTimeline){
            timelinePane.getChildren().add(box);
        }
    }
    
    /** last method in the process of showing timeline, adds all member of list 
     * to the pane on screen.
     * 
     */
    private void displaySearchTimeline() {
        mentionsDisplayed = false;
        timelineDisplayed = false;
        savedDisplayed = false;
        searchDisplayed = true;
        LOG.debug("displaying search timeline");
        timelinePane.getChildren().clear();
        for(VBox box : searchTimeline){
            timelinePane.getChildren().add(box);
        }
    }
    
    /** last method in the process of showing timeline, adds all member of list 
     * to the pane on screen.
     * 
     */
    private void displaySavedTimeline(){
        mentionsDisplayed = false;
        timelineDisplayed = false;
        savedDisplayed = true;
        searchDisplayed = false;
        LOG.debug("displaying saved timeline");
        timelinePane.getChildren().clear();
        for(VBox box : savedTimeline){
            timelinePane.getChildren().add(box);
        }
    }
    

    
    /** invoked at the end of the process of showing a timeline. this method resets
     * the button text of the button that was NOT pressed so that it is clear that 
     * its timeline is reset
     * 
     */
    private void updateButtonText() {
        if(mentionsDisplayed){
            showTimelineBtn.setText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("timelineButton"));
            showMentionsBtn.setText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("20more"));
        }
        if(timelineDisplayed){
            showMentionsBtn.setText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("mentionsButton"));
            showTimelineBtn.setText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("20more"));
        }
        if(savedDisplayed){
            showMentionsBtn.setText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("mentionsButton"));
            showTimelineBtn.setText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("timelineButton"));
        }
        if(searchDisplayed){
            showMentionsBtn.setText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("mentionsButton"));
            showTimelineBtn.setText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("timelineButton"));
        }
    }

    
    /** switch pane to retweet pane
     * 
     * @param id 
     */
    private void showRetweetPaneForId(TweetData id) {
        hideAllBut(retweetPane);
        currentlyInteractingWith = id;
    }

    
    /**
     *  method used to switch between panes. hides all bu the pane given as input
     * @param pane 
     */
    private void hideAllBut(Pane pane) {
        showUserPane.setVisible(false);
        homePane.setVisible(false);
        retweetPane.setVisible(false);
        pane.setVisible(true);
    }

    
    /** handler for the profile picture click event. will switch panes with the
     * appropriate one and set the data of the the user on that pane.
     * 
     * @param imagePattern
     * @param tweet 
     */
    private void showUserDetails(ImagePattern imagePattern, TweetData tweet) {
        LOG.debug("displaying user details for tweet" + tweet);
        currentlyInteractingWith = tweet;
        User userToDisplay = tweet.getUser();
        showUserName.setText(userToDisplay.getName());
        showUserScreenName.setText(userToDisplay.getScreenName());
        
        userProfileImage.setFill(imagePattern);
        
        isFollowingCurrentlyInteractingWith = userToDisplay.isFollowRequestSent();
        
        if(isFollowingCurrentlyInteractingWith){
            followBtn.setTextFill(Color.BLACK);
        }
        else{
            followBtn.setTextFill(Color.BLUE);
        }
        
        followBtn.setOnAction(e -> handleFollowButton(userToDisplay));
        
        hideAllBut(showUserPane);
    }

    /**
     *  event handler for follow button click
     * @param user 
     */
    private void handleFollowButton(User user) {
        try {
        if(isFollowingCurrentlyInteractingWith){
            TM.unfollow(user.getId());
            followBtn.setTextFill(Color.BLACK);
            isFollowingCurrentlyInteractingWith = false;
        }
        else{
            TM.follow(user.getId());
            followBtn.setTextFill(Color.BLUE);
            isFollowingCurrentlyInteractingWith = true;
        }
        } catch (TwitterException ex) {
            LOG.debug("failed to follow / unfollow");
        }
    }

    public void setRootController(RootLayoutController controller){
        rootController = controller;
    }

    private void loadSearchTimeline() {
        List<TweetData> timelineSearchData;
        searchTimeline.clear();
        try {
            timelineSearchData = TM.search(timelineSearch.getText());
            LOG.debug("adding tweets to mention timeline...");
            for(TweetData tweet : timelineSearchData){
                addToTimeline(tweet,searchTimeline);
            }
            mentionsPage = 1;
            timelinePage = 1;
            timeline.clear();            
            savedTimeline.clear();
            mentionsTimeline.clear();
        } catch (TwitterException ex) {
            LOG.error("failed to get timeline", ex);
        }    
    }

    



}