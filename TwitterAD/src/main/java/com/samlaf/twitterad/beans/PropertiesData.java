/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samlaf.twitterad.beans;

/** This is the bean class to hold the data for the properties file.
 *
 * @author Sam
 */
public class PropertiesData {
    
    private String consumerKey;
    private String consumerSecret;
    private String accessToken;
    private String accessTokenSecret;
   
    public PropertiesData(){
        
    } 
    /** basic constructor
     * 
     * @param consumerKey
     * @param consumerSecret
     * @param accessToken
     * @param accessTokenSecret 
     */
    public PropertiesData(String consumerKey, String consumerSecret, String accessToken, String accessTokenSecret){
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.accessTokenSecret = accessTokenSecret;
        this.accessToken = accessToken;
    }
    
    public String getConsumerKey() {
        return consumerKey;
    }

    public String getConsumerSecret() {
        return consumerSecret;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getAccessTokenSecret() {
        return accessTokenSecret;
    }
    
    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setAccessTokenSecret(String accessTokenSecret) {
        this.accessTokenSecret = accessTokenSecret;
    }
}
