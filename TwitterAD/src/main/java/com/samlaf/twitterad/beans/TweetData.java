/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samlaf.twitterad.beans;

import java.util.Objects;
import java.util.logging.Logger;
import javafx.beans.property.StringProperty;
import twitter4j.User;

/**
 *
 * @author Sam
 */
public class TweetData {
    
    private static final Logger LOG = Logger.getLogger(TweetData.class.getName());
    
    private  Long id;
    private  User user;
    private String text;
    private Boolean isLiked;
    private Boolean isRetweeted;
    
    
    public TweetData(Long id, User user, String text,Boolean isLiked,Boolean isRetweeted){
        this.id = id;
        this.user = user;
        this.text = text;
        this.isLiked = isLiked;
        this.isRetweeted = isRetweeted;
    }

    public TweetData() {
        
        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.id);
        hash = 29 * hash + Objects.hashCode(this.user);
        hash = 29 * hash + Objects.hashCode(this.text);
        hash = 29 * hash + Objects.hashCode(this.isLiked);
        hash = 29 * hash + Objects.hashCode(this.isRetweeted);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TweetData other = (TweetData) obj;
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.isLiked, other.isLiked)) {
            return false;
        }
        if (!Objects.equals(this.isRetweeted, other.isRetweeted)) {
            return false;
        }
        return true;
    }
    
    

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public Boolean isLiked() {
        return isLiked;
    }

    public Boolean isRetweeted() {
        return isRetweeted;
    }

    public void setIsLiked(Boolean isLiked) {
        this.isLiked = isLiked;
    }

    public void setIsRetweeted(Boolean isRetweeted) {
        this.isRetweeted = isRetweeted;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    
    
}
