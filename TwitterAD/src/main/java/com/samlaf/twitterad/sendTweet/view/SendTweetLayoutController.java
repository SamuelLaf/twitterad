package com.samlaf.twitterad.sendTweet.view;

import com.samlaf.twitterad.business.TwitterManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * controller class FXML. handles the send Tweet page
 * @author Sam
 */
public class SendTweetLayoutController {
    
    private static final Logger LOG =  LoggerFactory.getLogger(SendTweetLayoutController.class);
    
    TwitterManager tw = new TwitterManager();

    @FXML // fx:id="tweetTextArea"
    private TextArea tweetTextArea; // Value injected by FXMLLoader
    
    @FXML
    private Label errorLabel;

    /**
     * send tweet handler
     * @param event 
     */

    @FXML
    void sendTweetHandler(ActionEvent event) {
        sendTweet();
        
        
    }
    /**
     * gets text from associated field and checks the length then post the tweet
     * using twitter4j library
     */
    private void sendTweet(){
        String text = tweetTextArea.getText();
        if(text.length() > 280){
            errorLabel.setVisible(true);
        }
        try {
            tw.SendTweet(text);
            errorLabel.setVisible(false);
        } catch (TwitterException ex) {
            errorLabel.setVisible(true);
            LOG.info("failed to send tweet",ex);
        }
    }

}
