/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samlaf.twitterad.persistance;

import com.samlaf.twitterad.beans.TweetData;
import com.samlaf.twitterad.business.TwitterManager;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 *
 * @author 1731841
 */
public class TweetDAOImpl {
    
    
    private final static Logger LOG = LoggerFactory.getLogger(TweetDAOImpl.class);

    // This information should be coming from a Properties file
    private final static String URL = "jdbc:mysql://localhost:3306/TWITTERAD?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final static String USER = "student";
    private final static String PASSWORD = "secret";
    
    private final TwitterManager TM = new TwitterManager();
    
    
    public Boolean createTweet(TweetData tweet) throws SQLException{
        
        final String QUERRY= "INSERT INTO tweets (id,userId,text,favorited,retweeted) VALUES (?,?,?,?,?);";
        
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
                PreparedStatement ps = connection.prepareStatement(QUERRY);
                ){
            ps.setLong(1, tweet.getId());
            ps.setLong(2, tweet.getUser().getId()); 
            ps.setString(3,tweet.getText());
            ps.setBoolean(4, tweet.isLiked()); 
            ps.setBoolean(5, tweet.isRetweeted());  
            ps.execute();
            return true;
        }
    }
    
    
    public ArrayList<TweetData> getSavedTweets() throws SQLException, TwitterException{
        
        final String QUERRY= "SELECT * FROM tweets";
        
        try(Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
                PreparedStatement ps = connection.prepareStatement(QUERRY);
                ){
            ResultSet rs = ps.executeQuery();
            
            ArrayList<TweetData> tweets = new ArrayList();
            while(rs.next()){
                TweetData tweet = new TweetData();
                tweet.setId(rs.getLong(1));
                tweet.setUser(TM.getUserDetails(rs.getLong(2)));
                tweet.setText(rs.getString(3));
                tweet.setIsLiked(rs.getBoolean(4));
                tweet.setIsRetweeted(rs.getBoolean(5));
                tweets.add(tweet);
            }
            return tweets;
        } 
        
        
        
    }
}
