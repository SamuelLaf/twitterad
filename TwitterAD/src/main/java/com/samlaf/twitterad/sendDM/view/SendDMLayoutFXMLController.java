package com.samlaf.twitterad.sendDM.view;

import com.samlaf.twitterad.business.TwitterManager;
import com.samlaf.twitterad.tweet.view.TweetLayoutController;
import com.samlaf.twitterad.view.RootLayoutController;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.ResponseList;
import twitter4j.TwitterException;
import twitter4j.User;


/**
 * controller for the pane dedicated to sending direct messages 
 * @author Sam
 */
public class SendDMLayoutFXMLController implements Initializable {
    
    private static final Logger LOG = LoggerFactory.getLogger(SendDMLayoutFXMLController.class);
    private final TwitterManager TM = new TwitterManager();
    private Long currentlyChattingWith;
    private RootLayoutController rootController;
    
    /** Long = id of sender, list = the messages they sent
     * 
     */
    private Map<Long,ArrayList<DirectMessage>> savedDirectmessages = new HashMap<>();
    
    
    
    private ArrayList<HBox> conversationList = new ArrayList<>();
    
    private final Long CLIENT_ID = TM.getCurrentUserId();

    @FXML
    private VBox dmUserList;

    @FXML
    private VBox dmMessageList;

    @FXML
    private TextField dMTextfield;

    @FXML
    private Button sendDMButton;
    
    @FXML
    private BorderPane messagingPane;
    

    
    /**
     * send button handler uses twitter4j library to send DM
     * @param event 
     */
    @FXML
    void sendDMHandler(ActionEvent event) {
        
        String text = dMTextfield.getText();
        try {
            DirectMessage msg =TM.SendDM(text, currentlyChattingWith);
            dMTextfield.setText("");
            
            addToConvo(msg, currentlyChattingWith);
            displayConvoFor(currentlyChattingWith);
            
        } catch (TwitterException ex) {
            LOG.debug("failed to send DM");
        }
        
    }
    
    /**
     * handler for update button
     * @param event 
     */
    @FXML
    void updateButton(ActionEvent event) {
        updateData();
        messagingPane.setVisible(false);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LOG.debug("loading Dms");
        
        updateData();
        
    }
    
    /**
     * loads data from API manages the process of loading the data received onto 
     * the screen
     */
    private void updateData(){
        loadDMs();
        createConversationList();
        displayConversationList();
    }

    

    /**
     * first method in the process of displaying DM. gets data from API.
     * calls addToList on all of them
     */
    private void loadDMs() {
        LOG.debug("loading dm from twitter");
        savedDirectmessages.clear();
        try {
            ResponseList<DirectMessage> dmList = TM.getDmList();

            for(DirectMessage dm : dmList){
                addToList(dm);
                
            }
        } catch (TwitterException ex) {
            LOG.error("failed to get Dms");
        }
        
        LOG.debug("saved DMs are loaded:" + savedDirectmessages);
    }
    
    /**
     * adds the DM given as input into the list associated with the user sent to / received from
     * depending on if the sender is the logged in user. 
     * @param dm 
     */
    private void addToList(DirectMessage dm) {
        Long id = dm.getSenderId();
        LOG.debug(dm.getRecipientId() + " compared to "+ CLIENT_ID + "...");
        if(CLIENT_ID.equals(id)){
            id = dm.getRecipientId();
        }
        
        if(savedDirectmessages.containsKey(id)){
            savedDirectmessages.get(id).add(dm);
        }
        else{
            ArrayList<DirectMessage> dmList = new ArrayList<DirectMessage>();
            dmList.add(dm);
            savedDirectmessages.put(id, dmList);
        }
    }

    /** creates the conversation list based on the keys from the previously 
     * list usingFXML
     * 
     */
    private void createConversationList() {
        LOG.debug("creating convo list");
        conversationList.clear();
        
        for(Long receipientId : savedDirectmessages.keySet()){
            
        
        
            try{

                FXMLLoader load = new FXMLLoader();

                Locale location = new Locale("en", "CA");

                load.setLocation(getClass().getResource("/fxml/dmConversationLayoutFXML.fxml"));
                load.setResources(ResourceBundle.getBundle("MessagesBundle", location));

                HBox conversationBox = load.load();
                
                DmConversationLayoutFXMLController controller = load.getController();
                
                String name = TM.getName(receipientId);
                Image image = new Image(TM.getPicUrl(receipientId));
                
                controller.setId(receipientId);
                controller.setName(name);
                controller.setIconFill(new ImagePattern(image));
                
                conversationBox.setOnMouseClicked(e -> displayConvoFor(receipientId));
                
                
                LOG.debug("added " + name + " to convo list");
                conversationList.add(conversationBox);

            }catch(IOException e){
                LOG.error(null, e);
                Platform.exit();
            } catch (TwitterException ex) {
                LOG.error("failed to get screen name of user", ex);
            }
        }
    }

    /**
     * displays the conversation list to the screen
     */
    private void displayConversationList() {
        dmUserList.getChildren().clear();
        for(HBox conversation : conversationList){
            dmUserList.getChildren().add(conversation);
        }
    }
    
    /**
     * displays the DMs associated with a convo when a convo is clicked.
     * 
     * if the client sent it, display it on the left, else its on the right
     * @param convoId 
     */
    private void displayConvoFor(Long convoId){
        dmMessageList.getChildren().clear();
        LOG.debug("displaying convo for id: "+ convoId);
        ArrayList<DirectMessage> dmsToDisplay;
        if(savedDirectmessages.get(convoId) == null){
            dmsToDisplay = new ArrayList<DirectMessage>();
            savedDirectmessages.put(convoId, dmsToDisplay);
        }else{
            dmsToDisplay = new ArrayList<DirectMessage>(savedDirectmessages.get(convoId));
        }
        
        //sort to reverse the order since the newest are first in list from
        //twitter4j
        Collections.reverse(dmsToDisplay);
        
        for(DirectMessage dm : dmsToDisplay){
            Label text = new Label(dm.getText());
            text.setMaxWidth(messagingPane.getWidth());
            
            if(dm.getRecipientId() == CLIENT_ID){
                text.setAlignment(Pos.CENTER_LEFT);
            }
            else{
                text.setAlignment(Pos.CENTER_RIGHT);
            }
            dmMessageList.getChildren().add(text);
        }
        messagingPane.setVisible(true);
        currentlyChattingWith = convoId;
    }
    
    /** calls display convo for the clicked convo
     * 
     * @param id 
     */
    public void getConvoFor(Long id){
        if(savedDirectmessages.containsKey(id)){
        }
        else{
            savedDirectmessages.put(id, new ArrayList<>());
        }
        
        displayConvoFor(id);
    }
    
    
    /**
     * this method is used when the user sends a message to add it to the feed
     * @param msg
     * @param id 
     */
    public void addToConvo(DirectMessage msg,Long id){
        LOG.debug("adding" + msg.getText() + " to the convo with: " + id);
        savedDirectmessages.get(id).add(0, msg);
        
    }
    
    /**
     * determines if client is already chatting with new user
     * @param id
     * @return 
     */
    private Boolean alreadyChattingWith(Long id){
        if(savedDirectmessages.containsKey(id) ){
            return true;
        }
        return false;
    }
    
    public void createConvo(Long id){
        if(alreadyChattingWith(id)){
            displayConvoFor(id);
            return;
        }
        try{

                Long receipientId = id;
                FXMLLoader load = new FXMLLoader();

                Locale location = new Locale("en", "CA");

                load.setLocation(getClass().getResource("/fxml/dmConversationLayoutFXML.fxml"));
                load.setResources(ResourceBundle.getBundle("MessagesBundle", location));

                HBox conversationBox = load.load();
                
                DmConversationLayoutFXMLController controller = load.getController();
                
                String name = TM.getName(receipientId);
                Image image = new Image(TM.getPicUrl(receipientId));
                
                controller.setId(receipientId);
                controller.setName(name);
                controller.setIconFill(new ImagePattern(image));
                
                conversationBox.setOnMouseClicked(e -> displayConvoFor(receipientId));
                
                
                LOG.debug("added " + name + " to convo list");
                conversationList.add(conversationBox);

            }catch(IOException e){
                LOG.error(null, e);
                Platform.exit();
            } catch (TwitterException ex) {
                LOG.error("failed to get screen name of user", ex);
            }
        
        displayConversationList();
        displayConvoFor(id);
    }
    
    

}
