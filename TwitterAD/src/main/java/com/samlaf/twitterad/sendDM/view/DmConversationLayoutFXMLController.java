package com.samlaf.twitterad.sendDM.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

/**
 * controller for individual convos from the list in the send DM layout
 * @author Sam
 */
public class DmConversationLayoutFXMLController {

    @FXML // fx:id="icon"
    private Circle icon; // Value injected by FXMLLoader

    @FXML // fx:id="nameLbl"
    private Label nameLbl; // Value injected by FXMLLoader

    private Long id;

    public void setIconFill(Paint image) {
        this.icon.setFill(image);
    }

    public void setName(String name) {
        this.nameLbl.setText(name);
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    
}
