package com.samlaf.twitterad.tweet.view;

import com.samlaf.twitterad.beans.TweetData;
import com.samlaf.twitterad.business.TwitterManager;
import com.samlaf.twitterad.persistance.TweetDAOImpl;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.logging.Level;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * controller class FXML. handles the data for individual tweets
 * @author Sam
 */
public class TweetLayoutController {

    private static final Logger LOG = LoggerFactory.getLogger(TweetLayoutController.class);
    
    
    private int likeCount;
    private int rtCount;
    
    @FXML // fx:id="tweetName"
    private Label tweetName; // Value injected by FXMLLoader

    @FXML // fx:id="tweetText"
    private Label tweetText; // Value injected by FXMLLoader

    @FXML
    private Circle profileCircle;

    @FXML
    private MenuButton rtButton;
    
    @FXML
    private Button likeButton;
    
    @FXML
    private Button saveButton;
    
    @FXML
    private MenuItem rtWithTextBtn;
    
    
    private TweetData tweetBean;
    
    private final TwitterManager tm = new TwitterManager();
    
    /**
     *  handles reply button click
     * @param event 
     */
    @FXML
    void commentHandler(ActionEvent event) {
        LOG.debug("reply button");
    }
    
    @FXML
    void saveToDatabase(ActionEvent event){
        LOG.debug("saving to db tweet with id: " + tweetBean.getId());
        TweetDAOImpl dao = new TweetDAOImpl();
        try {
            dao.createTweet(tweetBean);
            disableSaveBtn();
        }catch (SQLIntegrityConstraintViolationException ex) {
            disableSaveBtn();
        } catch (SQLException ex) {
            LOG.error("failed to save to db", ex);
        }
    }

    /**
     *  like click handler, will switch between like / unlike for the tweet by 
     * posting with twitter4j
     * @param event 
     */
    @FXML
    void likeHandler(ActionEvent event) {
        LOG.debug("like btn clicked");
        try{
            if(tweetBean.isLiked()){
                if(tm.unlike(tweetBean.getId())){
                    tweetBean.setIsLiked(false);
                    likeButton.setTextFill(Color.BLACK);
                    likeButton.setText(Integer.toString(rtCount-1));
                }
                
            }
            else{
                if(tm.like(tweetBean.getId())){
                    tweetBean.setIsLiked(true);
                    likeButton.setTextFill(Color.RED);
                    likeButton.setText(Integer.toString(rtCount+1));
                    
                }
            }
        }
        catch(TwitterException ex){
            LOG.error("failed to like/ unlike",ex);
        }
        
    }
    
    

    
    /**
     *  retweet click handler, will switch between like / unlike for the tweet by 
     * posting with twitter4j
     * @param event 
     */
    @FXML
    void retweetHandler(ActionEvent event) {
        LOG.debug("retweet btn clicked");
        try{
            if(tweetBean.isRetweeted()){
                if(tm.unRetweet(tweetBean.getId())){
                    tweetBean.setIsRetweeted(false);
                    rtWithTextBtn.setDisable(false);
                    rtButton.setTextFill(Color.BLACK);
                    rtButton.setText(Integer.toString(rtCount-1));
                }
                
            }
            else{
                if(tm.retweet(tweetBean.getId())){
                    tweetBean.setIsRetweeted(true);
                    rtWithTextBtn.setDisable(true);
                    rtButton.setTextFill(Color.AQUA);
                rtButton.setText(Integer.toString(rtCount+1));
                    
                }
            }
        }
        catch(TwitterException ex){
            LOG.error("failed to retweet/ unretweet",ex);
        }
    }

    

    public void setTweetName(String tweetName) {
        this.tweetName.setText(tweetName);
    }

    public void setTweetText(String tweetText) {
        this.tweetText.setText(tweetText);
    }

    public void setProfileImage(Paint image) {
        profileCircle.setFill(image);
    }

    /**
     * setter for tweet. changes the color of button depending on their corresponding 
     * state
     * @param tweet 
     */
    public void setTweet(TweetData tweet) {
        tweetBean = tweet;
        
        if(tweet.isLiked()){
            likeButton.setTextFill(Color.RED);
        }
        if(tweet.isRetweeted()){
            rtWithTextBtn.setDisable(true);
            rtButton.setTextFill(Color.AQUA);
        }
        
    }
    
    public MenuItem getRtWithTextBtn() {
        return rtWithTextBtn;
    }

    public void setProfileCircleAction(EventHandler eh) {
        LOG.debug("set event handler for profile pic of " + tweetBean.getId());
        profileCircle.setOnMouseClicked(eh);
    }
    
    public void disableSaveBtn(){
        saveButton.setDisable(true);
    }
    
    public void setCounts(){
        TwitterManager tm = new TwitterManager();
        
        try {
            likeCount = tm.countLikes(tweetBean.getId());
            likeButton.setText(Integer.toString(likeCount));
            rtCount = tm.countRTs(tweetBean.getId());
            rtButton.setText(Integer.toString(rtCount));
        } catch (TwitterException ex) {
            java.util.logging.Logger.getLogger(TweetLayoutController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

    

    
    
    

}
