package com.samlaf.twitterad.view;

import com.samlaf.twitterad.business.Business;
import com.samlaf.twitterad.form.view.FormLayoutFXMLController;
import com.samlaf.twitterad.home.view.HomeLayoutController;
import com.samlaf.twitterad.sendDM.view.SendDMLayoutFXMLController;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  controller class FXML. root layout that will initiates all other layouts
 * @author Sam
 */
public class RootLayoutController {

    private static final Logger LOG = LoggerFactory.getLogger(RootLayoutController.class);

    private Locale location;
    private Business business;
    private ArrayList<Pane> panes = new ArrayList<Pane>();
    private SendDMLayoutFXMLController dmController;
    
   

    @FXML // fx:id="rootPane"
    private AnchorPane rootPane; // Value injected by FXMLLoader
    
    @FXML
    private ImageView showUserBtn;

    @FXML // fx:id="twitterPane"
    private BorderPane twitterPane; // Value injected by FXMLLoader

    @FXML // fx:id="buttonList"
    private VBox buttonList; // Value injected by FXMLLoader

    @FXML // fx:id="homeBtn"
    private ImageView homeBtn; // Value injected by FXMLLoader

    @FXML // fx:id="sendTweetBtn"
    private ImageView sendTweetBtn; // Value injected by FXMLLoader

    @FXML // fx:id="sendDMBtn"
    private ImageView sendDMBtn; // Value injected by FXMLLoader

    @FXML // fx:id="aboutBtn"
    private ImageView aboutBtn; // Value injected by FXMLLoader

    @FXML // fx:id="exitBtn"
    private ImageView exitBtn; // Value injected by FXMLLoader

    @FXML // fx:id="homeLayout"
    private AnchorPane homeLayout; // Value injected by FXMLLoader

    @FXML // fx:id="sendTweetPane"
    private AnchorPane sendTweetPane; // Value injected by FXMLLoader
 
    @FXML // fx:id="sendTweetPane"
    private AnchorPane sendDMPane; // Value injected by FXMLLoader
    
    @FXML
    private AnchorPane formPane;

    @FXML
    private AnchorPane showUserPane;    
    
    @FXML AnchorPane homePane;

    
    /**
     * handlers for side buttons.
     * hide all but the associated pane
     * @param event 
     */
    @FXML
    void homeBtnHandler(MouseEvent event) {
        hideAllBut(homePane);
    }

    @FXML
    void showTweetHandler(MouseEvent event) {
        hideAllBut(sendTweetPane);
    }

    @FXML
    void showDMHandler(MouseEvent event) {
        hideAllBut(sendDMPane);
    }
    
    @FXML
    void showUserHandler(MouseEvent event){
        
    }
    
    @FXML
    void aboutHandler(MouseEvent event) {

    }

    @FXML
    void exitHandler(MouseEvent event) {
        Platform.exit();

    }

    /**
     * the only pane that can be intiated safely as other might require the keys
     */
    @FXML
    void initialize() {
        initFormLayout();
    }
    
    /**
     * initiates homelayout from FXML
     */
    private void initHomeLayout() {
        try{
            
            FXMLLoader load = new FXMLLoader();
            location = new Locale("en", "CA");
            
            
            load.setLocation(getClass().getResource("/fxml/homeLayoutFXML.fxml"));
            load.setResources(ResourceBundle.getBundle("MessagesBundle", location));
            
            AnchorPane root = (AnchorPane) load.load();
            
            HomeLayoutController homeController = load.getController();
            homeController.setRootController(this);
            
            homePane.getChildren().add(root);
            
        }catch(IOException e){
            LOG.error(null, e);
            Platform.exit();
        }
    }
    
    
    /**
     * initiates formLayout from FXML
     */
    private void initFormLayout(){
        try{
            
            FXMLLoader load = new FXMLLoader();
            location = new Locale("en", "CA");
            
            
            load.setLocation(getClass().getResource("/fxml/formLayoutFXML.fxml"));
            load.setResources(ResourceBundle.getBundle("MessagesBundle", location));
            
            AnchorPane root = (AnchorPane) load.load();
            
            FormLayoutFXMLController formControl = load.getController();
            
            formControl.setRootControl(this);
            
            formPane.getChildren().add(root);
            
            
        }catch(IOException e){
            LOG.error(null, e);
            Platform.exit();
        }
    }

    
    /**
     * initiates initSendTweetLayout from FXML
     */
    private void initSendTweetLayout() {
        try{
            
            FXMLLoader load = new FXMLLoader();
            location = new Locale("en", "CA");
            
            
            load.setLocation(getClass().getResource("/fxml/sendTweetLayoutFXML.fxml"));
            load.setResources(ResourceBundle.getBundle("MessagesBundle", location));
            
            AnchorPane root = (AnchorPane) load.load();
            
            sendTweetPane.getChildren().add(root);
            
        }catch(IOException e){
            LOG.error(null, e);
            Platform.exit();
        }
    }
    
    
    /**
     * initiates initSendDMLayout from FXML
     */
    private void initSendDMLayout(){
        try{
            
            FXMLLoader load = new FXMLLoader();
            location = new Locale("en", "CA");
            
            
            load.setLocation(getClass().getResource("/fxml/sendDMLayoutFXML.fxml"));
            load.setResources(ResourceBundle.getBundle("MessagesBundle", location));
            
            AnchorPane root = (AnchorPane) load.load();
            
            sendDMPane.getChildren().add(root);
            
            dmController = load.getController();
            
            
        }catch(IOException e){
            LOG.error(null, e);
            Platform.exit();
        }
    }
 
    
    public void setBusiness(Business business) {
        this.business = business;
    }

    /**
     * hides all but the pane from parameters
     * @param pane 
     */
    private void hideAllBut(Pane pane) {
        hideAll();
        pane.setVisible(true);
        pane.toFront();
    }

    /**
     * hides all panes so that for mcan be displayed
     */
    private void hideAll() {
        panes.forEach((pane) -> {
            pane.setVisible(false);
            
        });
    }
    
    /**
     * when form is accepted, show twitter
     */
    public void showTwitter(){
        
        initHomeLayout();
        initSendTweetLayout();
        initSendDMLayout();
        panes.add(formPane);
        panes.add(homePane);
        panes.add(sendTweetPane);
        panes.add(sendDMPane);
        panes.add(showUserPane);
        LOG.debug("init root done");
        
        LOG.debug("show Twitter");
        twitterPane.setVisible(true);
        hideAllBut(twitterPane);
        hideAllBut(homePane);
        
        
    }

    /**
     * shows form when properties are rejected
     */
    public void showForm() {
        LOG.debug("display form");
        twitterPane.setVisible(false);
        hideAllBut(formPane);
    }
    
    public void switchToConvo(Long id){
        dmController.createConvo(id);
        hideAllBut(sendDMPane);
    }
    
    

    
    
    
}
