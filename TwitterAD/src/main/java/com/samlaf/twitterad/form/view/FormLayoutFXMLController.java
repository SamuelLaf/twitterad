/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samlaf.twitterad.form.view;

import com.samlaf.twitterad.business.Business;
import com.samlaf.twitterad.view.RootLayoutController;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class. This class will manage the input in the form and 
 * display a message when the enttries are invalid.
 *
 * @author 1731841
 */
public class FormLayoutFXMLController implements Initializable {

    private Business business = new Business();
    private RootLayoutController rootControl;
    
    private static final Logger LOG = LoggerFactory.getLogger(FormLayoutFXMLController.class);


    @FXML
    private GridPane formPane;

    @FXML
    private TextField consumerKeyField;

    @FXML
    private TextField consumerSecretField;

    @FXML
    private TextField accessTokenField;

    @FXML
    private TextField accessTokenSecretField;

    @FXML
    private Label errorLabel;

    @FXML
    private Button formSaveButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    /**
     * handler for the form's save button. tries to put the values into the 
     * data bean, will not continue until values are good
     * @param event 
     */
    public void saveFormBtn(ActionEvent event){
        LOG.debug("save btn handler");
        String consumerKey = consumerKeyField.getText();
        String consumerKeySecret = consumerSecretField.getText();
        String accessToken = accessTokenField.getText();
        String accesstokenSecret = accessTokenSecretField.getText();
        
        business.createPropertiesFile(consumerKey, consumerKeySecret, accessToken, accesstokenSecret);
        
        if(business.isPropertiesFileOk()){
            business.loadProperties();
            rootControl.showTwitter();
        }else{
            LOG.debug("created properties file was NOT ok");
            errorLabel.setVisible(true);
        }
    }

    public void setRootControl(RootLayoutController rootControl) {
        this.rootControl = rootControl;
        
    }
    
    
    
    
}
