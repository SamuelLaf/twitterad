/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samlaf.twitterad.presentation;

import com.samlaf.twitterad.business.Business;
import com.samlaf.twitterad.view.RootLayoutController;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Main method class. starts initiates the rootLayout, which initiates the rest.
 * 
 *
 * @author Sam
 */
public class GuiStart extends Application {
    
    //attributes
    private Stage stage;
    private AnchorPane rootLayout;
    private Locale location;
    private Business bs = new Business();
    private RootLayoutController control;
    
    //logger
    private final static Logger LOG = LoggerFactory.getLogger(GuiStart.class);
    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    /**
     * method that initiates the layout sets the stage and shows it.
     */
    public void start(Stage stage) throws Exception {
        
        this.stage = stage;
        location = new Locale("en", "CA");
        stage.setTitle(ResourceBundle.getBundle("MessagesBundle", location).getString("Title"));
        //initialize from RootLayout.fxml
        rootLayout = getRoot();
        
        
        //if the properties are NOT good, show form
        if(bs.isPropertiesFileOk()){
            control.showTwitter();
        }
        else{
            control.showForm();
            Path propFile = get("src//main//resources","keys.properties");
            Files.deleteIfExists(propFile);
        }
        Scene scene = new Scene(rootLayout);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }
    
    /**
     * get Root from fxml file, initiates root.
     * @return root
     */
    private AnchorPane getRoot(){
        
        
        try{
            
            FXMLLoader load = new FXMLLoader();
            
            load.setLocation(getClass().getResource("/fxml/rootLayoutFXML.fxml"));
            load.setResources(ResourceBundle.getBundle("MessagesBundle", location));
            
            AnchorPane root = (AnchorPane) load.load();
            
            control = load.getController();
            control.setBusiness(bs);
            return root;
            
        }catch(IOException e){
            LOG.error(null, e);
            Platform.exit();
            //for compiler to calm down
            return null;
        }
    }

    
    
    
}
