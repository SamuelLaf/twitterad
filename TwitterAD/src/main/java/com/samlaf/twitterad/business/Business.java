/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samlaf.twitterad.business;
import com.samlaf.twitterad.beans.PropertiesData;
import com.samlaf.twitterad.view.RootLayoutController;
import java.io.File;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import static java.nio.file.Paths.get;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** This class will take care of the computation that doesnt have anything to do
 * with the UI. It interacts with files to set the properties
 *
 * @author sam
 */
public class Business {
    
    private Properties prop;
    private PropertiesData propertiesData;
    Path propFile = get("twitter4j.properties");

    public PropertiesData getPd() {
        return propertiesData;
    }

    
     private static final Logger LOG = LoggerFactory.getLogger(TwitterManager.class);   
    
     
     /** This method is used to check the state of the twitter4j properties file
      * it will return false if the file does not exist, if it doesn't have the 
      * properties needed or if the properties are empty
      * 
      * @return 
      */
    public Boolean isPropertiesFileOk(){
         prop = new Properties();
         
         
         //check if file exists. if it does, load properties
         //else,create it and show form
         if(Files.exists(propFile)){
             try (InputStream propFileStream = newInputStream(propFile);) {
                prop.load(propFileStream);
            } catch (IOException ex) {
                 LOG.error("exception loading key file",ex);
                return false;
             }
             LOG.debug("prop file loaded into prop");
         }else{
             LOG.debug("propfile not found");
             return false;
         }
         //if one of the properties is empty, open form
         if(prop.getProperty("oauth.consumerKey").trim() == null ||
            prop.getProperty("oauth.consumerKey").trim().length() == 0 ||
            prop.getProperty("oauth.consumerSecret").trim() == null ||
            prop.getProperty("oauth.consumerSecret").trim().length() == 0 ||
            prop.getProperty("oauth.accessToken").trim() == null ||
            prop.getProperty("oauth.accessToken").trim().length() == 0 ||
            prop.getProperty("oauth.accessTokenSecret").trim() == null ||
            prop.getProperty("oauth.accessTokenSecret").trim().length() == 0 ){
             LOG.debug("properties file had bad strings");
             return false;
         }
         else{
             LOG.debug("keys had smtg in them");
         }
         LOG.debug("keys were ok");
         return true;
    }
    
    /** This method loads the properties into the propertiesData
     * 
     */
    public void loadProperties(){
        String consumerKey = prop.getProperty("oauth.consumerKey");
        String consumerSecret = prop.getProperty("oauth.consumerSecret");
        String accessToken = prop.getProperty("oauth.accessToken");
        String accessTokenSecret = prop.getProperty("oauth.accessTokenSecret");
        propertiesData = new PropertiesData(consumerKey, consumerSecret, accessToken, accessTokenSecret);
        LOG.debug("loaded properties into bean");
    }
    
    
    /** This method is used to create a properties file. it will not always be a
     * valid file as it is used to then call isPropertiesFileOk on this file with
     * the user's input from the form
     * 
     * @param consumerKey
     * @param consumerSecret
     * @param accessToken
     * @param accessTokenSecret 
     */
    public void createPropertiesFile(String consumerKey, String consumerSecret,String accessToken, String accessTokenSecret){
        
        
        
        List<String> lines = Arrays.asList(
                "oauth.consumerKey="+consumerKey, 
                "oauth.consumerSecret="+consumerSecret,
                "oauth.accessToken="+accessToken,
                "oauth.accessTokenSecret="+accessTokenSecret);
        try {
            if(Files.exists(propFile)){
                Files.delete(propFile);
            }
            Files.createFile(propFile);
            Files.write(propFile, lines,StandardCharsets.UTF_8);
            LOG.debug("file created properly");
            
        } catch (IOException ex) {
            LOG.error("failed to create file",ex);
        }
    }
    
    
    
}
