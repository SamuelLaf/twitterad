/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samlaf.twitterad.business;

import com.samlaf.twitterad.beans.TweetData;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.DirectMessage;
import twitter4j.DirectMessageList;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/** This class will manage the interaction with the twitter4j library
 *
 * @author 1731841
 * @throws twitter4j.TwitterException if connection fails or if the action
     * is denied
 */
public class TwitterManager {
    
    
    private final static Logger LOG = LoggerFactory.getLogger(TwitterManager.class);
    
    

    /** create Twitter singleton
     * 
     * @return getSingleton
     */
    
    public Twitter getTwitter(){
        Twitter twitter = TwitterFactory.getSingleton();
        return twitter;
    }
    
    /**posts a tweet 
     * 
     * @param text
     * @return
     * @throws TwitterException 
     */
    public String SendTweet(String text) throws TwitterException{
        
        LOG.debug("Sending tweet: " +text);
        
        Twitter twitter = getTwitter();
        Status status = twitter.updateStatus(text);
        return status.getText();
    }
    
   
    
    /**posts a direct message to the specified user
     * 
     * @param text
     * @param id
     * @return DirectMessage
     * @throws twitter4j.TwitterException 
     */
    public DirectMessage SendDM(String text, Long id) throws TwitterException{
        Twitter twitter = getTwitter();
        DirectMessage msg = twitter.sendDirectMessage(id, text);
        LOG.debug("sent dm: "+ text +" to " + id);
        return msg;
    }
    
    /** gets the list of direct messages
     * 
     * @return
     * @throws TwitterException 
     */
    public ResponseList<DirectMessage> getDmList() throws TwitterException{
        Twitter twitter = getTwitter();
        ResponseList<DirectMessage> list = twitter.getDirectMessages(50);
        
        return list;
    }
    
    
    /**gets home timeline at page mentioned
     * 
     * @param page
        * @return 
     * @throws twitter4j.TwitterException 
  */
    public List<TweetData> getTL(int page) throws TwitterException{
        Twitter twitter = getTwitter();
        
        Paging pageToGet = new Paging();
        pageToGet.setCount(20);
        pageToGet.setPage(page);
        
        List<Status> timeLineTweets = twitter.getHomeTimeline(pageToGet);
        List<TweetData> timeLineTweetsBeaned = new ArrayList<TweetData>();
        for(Status tweet : timeLineTweets){
            timeLineTweetsBeaned.add(new TweetData(tweet.getId(), tweet.getUser(), tweet.getText(), tweet.isFavorited(), tweet.isRetweeted()));
        }
        
        LOG.debug("received timeline");
        return timeLineTweetsBeaned;
    }
    
    
    /**gets mentions timeline at page mentioned
     * 
     * @param page
        * @return 
     * @throws twitter4j.TwitterException 
    */
    public List<TweetData> getMentionsTL(int page) throws TwitterException{
        Twitter twitter = getTwitter();
        
        Paging pageToGet = new Paging();
        pageToGet.setCount(20);
        pageToGet.setPage(page);
        
        List<Status> timeLineTweets = twitter.getMentionsTimeline(pageToGet);
        List<TweetData> timeLineTweetsBeaned = new ArrayList<TweetData>();
        for(Status tweet : timeLineTweets){
            timeLineTweetsBeaned.add(new TweetData(tweet.getId(), tweet.getUser(), tweet.getText(), tweet.isFavorited(), tweet.isRetweeted()));
        }
        LOG.debug("received mentions timeline timeline");
        return timeLineTweetsBeaned;
    }
    
    public ArrayList<TweetData> search(String text) throws TwitterException{
        Twitter twitter = getTwitter();
        
        Query query = new Query(text);
        
        ArrayList<TweetData> found = new ArrayList<>();
        for(Status tweet : twitter.search(query).getTweets()){
            found.add(new TweetData(tweet.getId(), tweet.getUser(), tweet.getText(), tweet.isFavorited(), tweet.isRetweeted()));
        }
        return found;
    }
    
    
    /** gets search results based on twitter's search algorithm
     * 
     * @param searchTxt
     * @throws TwitterException 
     */
    public QueryResult getSearchResults(String searchTxt) throws TwitterException{
        Twitter twitter = getTwitter();
        //create querry since search method only takes querries as param
        Query query = new Query(searchTxt);
        LOG.debug("received search results");
        return twitter.search(query);
        
    }
    

    /** get the user object from a screen name ( the @)
     * 
     * @param screenName
     * @return
     * @throws TwitterException 
     */
    public User getUserDetails(String screenName) throws TwitterException{
        Twitter twitter = getTwitter();
        return twitter.showUser(screenName);
    }
    
    /**
     * gets like count for tweet
     * @param id
     * @return
     * @throws TwitterException 
     */
    public int countLikes(Long id) throws TwitterException{
        Twitter twitter = getTwitter();
        return twitter.showStatus(id).getFavoriteCount();
    }
    
    /**
     * gets RT count for tweet
     * @param id
     * @return
     * @throws TwitterException 
     */
    public int countRTs(Long id) throws TwitterException{
        Twitter twitter = getTwitter();
        return twitter.showStatus(id).getRetweetCount();
    }
    
    
    /** get the user object from an id 
     * 
     * @param id
     * @return
     * @throws TwitterException 
     */
    public User getUserDetails(Long id) throws TwitterException{
        Twitter twitter = getTwitter();
        return twitter.showUser(id);
    }
    
    /** post request retweet of the specified tweet 
     * 
     * @param tweetId
     * @return
     * @throws TwitterException  if tweet is retweeted
     */
    public Boolean retweet(long tweetId) throws TwitterException{
        LOG.debug("retweeting tweet");
        Twitter twitter = getTwitter();
        twitter.retweetStatus(tweetId);
        return true;
    }
    
    
    /** post request to unretweet a tweet
     * 
     * @param tweetId
     * @return
     * @throws TwitterException if tweet is not retweeted
     */
    public Boolean unRetweet(long tweetId) throws TwitterException{
        
        LOG.debug("unretweeting tweet");
        Twitter twitter = getTwitter();
        twitter.unRetweetStatus(tweetId);
        return true;
    }
    
    /** posts request to like a tweet
     * 
     * @param tweetId
     * @return
     * @throws TwitterException 
     */
    public Boolean like(long tweetId) throws TwitterException{
        LOG.debug("liking tweet with id:" + tweetId);
        Twitter twitter = getTwitter();
        twitter.createFavorite(tweetId);
        return true;
    }
    
    /** posts request to unlike a tweet
     * 
     * @param tweetId
     * @return
     * @throws TwitterException 
     */
    public Boolean unlike(long tweetId) throws TwitterException{
        LOG.debug("unliking tweet");
        Twitter twitter = getTwitter();
        twitter.destroyFavorite(tweetId);
        return true;
    }
    /*
    public List<Status> getComments(long tweetId){
        LOG.debug("getting comments for tweet with id"+ tweetId);
        Twitter twitter = getTwitter();
        list<Status> comments = twitter.
        
    }
    */

    /** gets screen name of the user
     * 
     * @param receipientId
     * @return
     * @throws TwitterException 
     */
    public String getName(Long receipientId) throws TwitterException {
        Twitter twitter = getTwitter();
        
        User user = twitter.showUser(receipientId);
        
        return user.getScreenName();
    }
    
    
    /** gets profile picture of the specified user
     * 
     * 
     * @param receipientId
     * @return
     * @throws TwitterException 
     */
    public String getPicUrl(Long receipientId) throws TwitterException{
        Twitter twitter = getTwitter();
        
        User user = twitter.showUser(receipientId);
        
        return user.get400x400ProfileImageURL();
    }

    
    /** get the id of the user using the app
     * 
     * @return 
     */
    public Long getCurrentUserId() {
        Twitter twitter = getTwitter();
        try {
            return twitter.getId();
        } catch (TwitterException ex) {
            LOG.error( null, ex);
            return null;
        } catch (IllegalStateException ex) {
            LOG.error( null, ex);
            return null;
        }
    }
    
    
    /** post request to follow specified user
     * 
     * @param id
     * @throws TwitterException 
     */
    public void follow(Long id) throws TwitterException{
        Twitter twitter = getTwitter();
        
        twitter.createFriendship(id);
    }
    
    /** post request to unfollow specified user
     * 
     * @param id
     * @throws TwitterException 
     */
    public void unfollow(Long id) throws TwitterException{
        Twitter twitter = getTwitter();
        
        twitter.destroyFriendship(id);
    }
    
    
}
